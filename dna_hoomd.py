import hoomd
import hoomd.md
from hoomd import deprecated
import numpy as np


hoomd.context.initialize("")

system = deprecated.init.read_xml(filename="dna.hoomdxml")
snap = system.take_snapshot(all=True)
nl = hoomd.md.nlist.cell()


# Energy Parameters in kcal/mol
eps = 0.26
eps_gc = 4 * eps
eps_at = 2/3 * eps_gc
k1 = eps
k2 = 100 * eps
kth = 400 * eps
kphi = 4 * eps

# Bond Parameters in Å
d0_s5p = 3.899
d0_s3p = 3.559
d0_sa = 6.430
d0_st = 4.880
d0_sc = 4.921
d0_sg = 6.392


# Angle Parameters in radians
theta_s5p3s = np.deg2rad(94.49)
theta_p5s3p = np.deg2rad(120.15)
theta_p5sa = np.deg2rad(113.13)
theta_p3sa = np.deg2rad(108.38)
theta_p5st = np.deg2rad(102.79)
theta_p3st = np.deg2rad(112.72)
theta_p5sc = np.deg2rad(103.49)
theta_p3sc = np.deg2rad(112.39)
theta_p5sg = np.deg2rad(113.52)
theta_p3sg = np.deg2rad(108.12)
phi_p5s3p5s = np.deg2rad(-154.8)
phi_s3p5s3p = np.deg2rad(-179.17)
phi_as3p5s = np.deg2rad(-22.60)
phi_s3p5sa = np.deg2rad(50.69)
phi_ts3p5s = np.deg2rad(-33.42)
phi_s3p5st = np.deg2rad(54.69)
phi_cs3p5s = np.deg2rad(-32.72)
phi_s3p5sc = np.deg2rad(54.50)
phi_gs3p5s = np.deg2rad(-22.30)
phi_s3p5sg = np.deg2rad(50.69) # This dihedral is not specified using s3p5sa


# Non-bonded Parameters in Å
d_cut = 6.86
sig_gc = 2.8694 
sig_at = 2.9002
sig_0mb = 2.0**(-1/6)  # mismatched bases
sig_0 = d_cut * 2**(-1/6)
sig_stack = 3.77  # average stacking distance x 2^(-1/6)

# Particle positions generated using dna_cg are in Å
# print(snap.particles.position)


#print(snap.bonds.types)

### Bond potential
def v_bond(r, rmin, rmax, k1, k2, r0):
    V = k1 * (r - r0)**2 + k2 * (r - r0)**4
    F = -2 * k1 * (r - r0) - 4 * k2 * (r - r0)**3
    return (V, F)


bond_table = hoomd.md.bond.table(width=1000)
bond_table.bond_coeff.set("P1-S1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_s5p))
bond_table.bond_coeff.set("P2-S2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_s5p))
bond_table.bond_coeff.set("P1-S2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_s3p))
bond_table.bond_coeff.set("P2-S1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_s3p))

bond_table.bond_coeff.set("Ab1-S1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sa))
bond_table.bond_coeff.set("S1-Tb1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_st))
bond_table.bond_coeff.set("Cb1-S1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sc))
bond_table.bond_coeff.set("Gb1-S1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sg))
bond_table.bond_coeff.set("Ab1-S2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sa))
bond_table.bond_coeff.set("S2-Tb1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_st))
bond_table.bond_coeff.set("Cb1-S2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sc))
bond_table.bond_coeff.set("Gb1-S2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sg))
bond_table.bond_coeff.set("Ab2-S1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sa))
bond_table.bond_coeff.set("S1-Tb2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_st))
bond_table.bond_coeff.set("Cb2-S1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sc))
bond_table.bond_coeff.set("Gb2-S1", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sg))
bond_table.bond_coeff.set("Ab2-S2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sa))
bond_table.bond_coeff.set("S2-Tb2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_st))
bond_table.bond_coeff.set("Cb2-S2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sc))
bond_table.bond_coeff.set("Gb2-S2", func=v_bond, rmin=0.2, rmax=9.0, coeff=dict(k1=k1,k2=k2,r0=d0_sg))


#print(snap.angles.types)

### Harmonic angle potential
angle_harm = hoomd.md.angle.harmonic()
angle_harm.angle_coeff.set("P1-S2-P2", k=1, t0=1)
angle_harm.angle_coeff.set("S1-P1-S2", k=1, t0=1)
angle_harm.angle_coeff.set("P1-S1-P2", k=1, t0=1)
angle_harm.angle_coeff.set("S1-P2-S2", k=1, t0=1)

angle_harm.angle_coeff.set("Ab1-S1-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Ab1-S2-P2", k=1, t0=1)
angle_harm.angle_coeff.set("Ab1-S2-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Ab1-S1-P2", k=1, t0=1)

angle_harm.angle_coeff.set("P1-S1-Tb1", k=1, t0=1)
angle_harm.angle_coeff.set("P2-S2-Tb1", k=1, t0=1)
angle_harm.angle_coeff.set("P1-S2-Tb1", k=1, t0=1)
angle_harm.angle_coeff.set("P2-S1-Tb1", k=1, t0=1)

angle_harm.angle_coeff.set("Cb1-S1-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Cb1-S2-P2", k=1, t0=1)
angle_harm.angle_coeff.set("Cb1-S2-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Cb1-S1-P2", k=1, t0=1)

angle_harm.angle_coeff.set("Gb1-S2-P2", k=1, t0=1)
angle_harm.angle_coeff.set("Gb1-S1-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Gb1-S1-P2", k=1, t0=1)
angle_harm.angle_coeff.set("Gb1-S2-P1", k=1, t0=1)

angle_harm.angle_coeff.set("Ab2-S1-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Ab2-S2-P2", k=1, t0=1)
angle_harm.angle_coeff.set("Ab2-S2-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Ab2-S1-P2", k=1, t0=1)

angle_harm.angle_coeff.set("P1-S1-Tb2", k=1, t0=1)
angle_harm.angle_coeff.set("P2-S2-Tb2", k=1, t0=1)
angle_harm.angle_coeff.set("P1-S2-Tb2", k=1, t0=1)
angle_harm.angle_coeff.set("P2-S1-Tb2", k=1, t0=1)

angle_harm.angle_coeff.set("Cb2-S1-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Cb2-S2-P2", k=1, t0=1)
angle_harm.angle_coeff.set("Cb2-S2-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Cb2-S1-P2", k=1, t0=1)

angle_harm.angle_coeff.set("Gb2-S2-P2", k=1, t0=1)
angle_harm.angle_coeff.set("Gb2-S1-P1", k=1, t0=1)
angle_harm.angle_coeff.set("Gb2-S1-P2", k=1, t0=1)
angle_harm.angle_coeff.set("Gb2-S2-P1", k=1, t0=1)


#print(snap.dihedrals.types)

### Harmonic dihedral potential
def harmonic(phi, k, phi0):
    V = k * (1 - np.cos(phi-phi0));
    F = k * np.sin(phi-phi0);
    return (V, F)

dtable = hoomd.md.dihedral.table(width=1000)
dtable.dihedral_coeff.set("S1-P2-S2-P1", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5s3p))
dtable.dihedral_coeff.set("S2-P1-S1-P2", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5s3p))
dtable.dihedral_coeff.set("S1-P1-S2-P2", func=harmonic, coeff=dict(k=kphi,phi0=phi_p5s3p5s))
dtable.dihedral_coeff.set("S2-P2-S1-P1", func=harmonic, coeff=dict(k=kphi,phi0=phi_p5s3p5s))

dtable.dihedral_coeff.set("S2-P1-S1-Ab1", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sa))
dtable.dihedral_coeff.set("S1-P2-S2-Ab1", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sa))
dtable.dihedral_coeff.set("S1-P1-S2-Ab1", func=harmonic, coeff=dict(k=kphi,phi0=phi_as3p5s))
dtable.dihedral_coeff.set("S2-P2-S1-Ab1", func=harmonic, coeff=dict(k=kphi,phi0=phi_as3p5s))

dtable.dihedral_coeff.set("S1-P1-S2-Tb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_ts3p5s))
dtable.dihedral_coeff.set("S2-P2-S1-Tb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_ts3p5s))
dtable.dihedral_coeff.set("S1-P2-S2-Tb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5st))
dtable.dihedral_coeff.set("S2-P1-S1-Tb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5st))

dtable.dihedral_coeff.set("S1-P1-S2-Cb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_cs3p5s))
dtable.dihedral_coeff.set("S2-P2-S1-Cb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_cs3p5s))
dtable.dihedral_coeff.set("S2-P1-S1-Cb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sc))
dtable.dihedral_coeff.set("S1-P2-S2-Cb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sc))

dtable.dihedral_coeff.set("S1-P2-S2-Gb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sg))
dtable.dihedral_coeff.set("S2-P1-S1-Gb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sg))
dtable.dihedral_coeff.set("S1-P1-S2-Gb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_gs3p5s))
dtable.dihedral_coeff.set("S2-P2-S1-Gb1", func=harmonic, coeff=dict(k=kphi,phi0=phi_gs3p5s))

dtable.dihedral_coeff.set("S2-P1-S1-Ab2", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sa))
dtable.dihedral_coeff.set("S1-P2-S2-Ab2", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sa))
dtable.dihedral_coeff.set("S1-P1-S2-Ab2", func=harmonic, coeff=dict(k=kphi,phi0=phi_as3p5s))
dtable.dihedral_coeff.set("S2-P2-S1-Ab2", func=harmonic, coeff=dict(k=kphi,phi0=phi_as3p5s))

dtable.dihedral_coeff.set("S1-P1-S2-Tb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_ts3p5s))
dtable.dihedral_coeff.set("S2-P2-S1-Tb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_ts3p5s))
dtable.dihedral_coeff.set("S1-P2-S2-Tb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5st))
dtable.dihedral_coeff.set("S2-P1-S1-Tb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5st))

dtable.dihedral_coeff.set("S1-P1-S2-Cb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_cs3p5s))
dtable.dihedral_coeff.set("S2-P2-S1-Cb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_cs3p5s))
dtable.dihedral_coeff.set("S2-P1-S1-Cb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sc))
dtable.dihedral_coeff.set("S1-P2-S2-Cb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sc))

dtable.dihedral_coeff.set("S1-P2-S2-Gb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sg))
dtable.dihedral_coeff.set("S2-P1-S1-Gb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_s3p5sg))
dtable.dihedral_coeff.set("S1-P1-S2-Gb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_gs3p5s))
dtable.dihedral_coeff.set("S2-P2-S1-Gb2", func=harmonic, coeff=dict(k=kphi,phi0=phi_gs3p5s))


rcut = 9.0

### Non-bonded stacking potential
v_stack = hoomd.md.pair.lj(rcut, nl)

v_stack.pair_coeff.set(system.particles.types, system.particles.types, epsilon=0.0, sigma=0.0, r_cut=0)

v_stack.pair_coeff.set("Ab1", "Ab1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Ab1", "Tb1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Ab1", "Cb1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Ab1", "Gb1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Tb2", "Tb2", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Tb2", "Ab2", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Tb2", "Gb2", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Tb2", "Cb2", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Tb1", "Tb1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Tb1", "Cb1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Tb1", "Gb1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Ab2", "Ab2", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Ab2", "Gb2", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Ab2", "Cb2", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Cb1", "Cb1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Cb1", "Gb1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Gb2", "Gb2", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Gb2", "Cb2", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Gb1", "Gb1", epsilon=eps, sigma=sig_stack) 
v_stack.pair_coeff.set("Cb2", "Cb2", epsilon=eps, sigma=sig_stack) 


### Non-bonded base-pairing potential
# Only applied to complementary base pairs, v_ex considers mismatched bases
def v_bp(r, rmin, rmax, sig, eps):
    V = 4 * eps * (5 * (sig/r)**12 - 6 * (sig/r)**10)
    F = 240 * eps * (sig**12/r**13 - sig**10/r**11)
    return (V, F)

bp_table = hoomd.md.pair.table(width=1000, nlist=nl)

bp_table.pair_coeff.set(system.particles.types, system.particles.types,
         func=v_bp, rmin=1, rmax=2, coeff=dict(sig=0, eps=0.0)) # zero out all pair interactions

bp_table.pair_coeff.set("Ab1","Tb2", func=v_bp, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_at, eps=eps_at))
bp_table.pair_coeff.set("Cb1","Gb2", func=v_bp, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_gc, eps=eps_gc))
bp_table.pair_coeff.set("Ab2","Tb1", func=v_bp, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_at, eps=eps_at))
bp_table.pair_coeff.set("Cb2","Gb1", func=v_bp, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_gc, eps=eps_gc))


### Non-bonded excluded volume potential
def v_ex(r, rmin, rmax, sig, eps):
    V = 4 * eps * ((sig/r)**12 - (sig/r)**10) + eps
    F = 4 * eps * (12 * sig**12/r**13 - 10 * sig**10/r**11)
    return (V, F)

ex_table = hoomd.md.pair.table(width=1000, nlist=nl)

ex_table.pair_coeff.set(system.particles.types,system.particles.types,
        func=v_ex, rmin=1, rmax=2, coeff=dict(sig=0,eps=0))

ex_table.pair_coeff.set("Ab1", "Ab2", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Ab1", "Gb2", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Ab1", "Cb2", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Tb2", "Tb1", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Tb2", "Cb1", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Tb2", "Gb1", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Tb1", "Gb2", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Tb1", "Cb2", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Ab2", "Cb1", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Ab2", "Gb1", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Cb1", "Cb2", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 
ex_table.pair_coeff.set("Gb2", "Gb1", func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0mb,eps=eps)) 

ex_table.pair_coeff.set("P2", "P2",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P2", "S2",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P2", "Tb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P2", "Ab2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P2", "Cb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P2", "Gb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P2", "Gb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P2", "Cb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S2", "S2",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S2", "Tb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S2", "Ab2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S2", "Cb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S2", "Gb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S2", "Gb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S2", "Cb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "P1",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "S1",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "Ab1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "Tb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "P2",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "S2",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "Tb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "Ab2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "Cb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "Gb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "Gb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("P1", "Cb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "S1",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "Ab1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "Tb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "P2",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "S2",   func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "Tb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "Ab2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "Cb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "Gb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "Gb1",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("S1", "Cb2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("Ab1", "P2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("Ab1", "S2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("Tb2", "P2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 
ex_table.pair_coeff.set("Tb2", "S2",  func=v_ex, rmin=0.2, rmax=9.0, coeff=dict(sig=sig_0,eps=eps)) 


# Long range electrostatics: V_qq is comparable to V_pppm
charged = system.particles.charged
pppm = charge.pppm(group=charged)


hoomd.md.integrate.mode_standard(dt=0.005)
all = hoomd.group.all()
# kT = 0.6, 0.4, 0.2 --> T ~ 300K, 200K, 100K, resp.  
hoomd.md.integrate.langevin(group=all, kT=0.6, seed=42)

hoomd.analyze.log(filename="dna-output.log",
        quantities=["potential_energy", "temperature"],
        period=100,
        overwrite=True)

hoomd.dump.gsd("trajectory.gsd", period=2e3, group=all, overwrite=True)

hoomd.run(1e6)
