# DNA CG
This code can be used to build single or double stranded dna according to the model detailed in [https://doi.org/10.1063/1.2431804](https://doi.org/10.1063/1.2431804), apply the forcefield, and run a simulation in [HOOMD-blue](https://hoomd-blue.readthedocs.io/en/stable/).
