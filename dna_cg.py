import mbuild as mb
import numpy as np
import os
import argparse
import warnings
import xml.etree.ElementTree as ET


warnings.simplefilter("ignore", UserWarning) 

class DA(mb.Compound):
    def __init__(self, step):
        super(DA, self).__init__()
        if step % 2 == 0:
            pname = "_P1"
            sname = "_S1"
        else:
            pname = "_P2"
            sname = "_S2"

        self.add(mb.Particle(name=pname, pos=[-0.0628, 0.8896, 0.2186]), label=pname)
        self.add(mb.Particle(name=sname, pos=[0.2365, 0.6568, 0.1280]), label=sname)
        self.add(mb.Particle(name="_Ab1", pos=[0.0575, 0.0516, 0.0051]), label="_Ab1")
        self.add_bond((self[sname], self["_Ab1"]))
        self.add_bond((self[sname], self[pname]))  


class DT(mb.Compound):
    def __init__(self, step):
        super(DT, self).__init__()
        if step % 2 == 0:
            pname = "_P1"
            sname = "_S1"
        else:
            pname = "_P2"
            sname = "_S2"

        self.add(mb.Particle(name=pname, pos=[-0.0628, 0.8896, 0.2186]), label=pname)
        self.add(mb.Particle(name=sname, pos=[0.2365, 0.6568, 0.1280]), label=sname)
        self.add(mb.Particle(name="_Tb1", pos=[0.0159, 0.2344, 0.0191]), label="_Tb1")
        self.add_bond((self[sname], self["_Tb1"]))
        self.add_bond((self[sname], self[pname]))  


class DG(mb.Compound):
    def __init__(self, step):
        super(DG, self).__init__()
        if step % 2 == 0:
            pname = "_P1"
            sname = "_S1"
        else:
            pname = "_P2"
            sname = "_S2"

        self.add(mb.Particle(name=pname, pos=[-0.0628, 0.8896, 0.2186]), label=pname)
        self.add(mb.Particle(name=sname, pos=[0.2365, 0.6568, 0.1280]), label=sname)
        self.add(mb.Particle(name="_Gb1", pos=[0.0628, 0.0540, 0.0053]), label="_Gb1")
        self.add_bond((self[sname], self["_Gb1"]))
        self.add_bond((self[sname], self[pname]))  
        
        
class DC(mb.Compound):
    def __init__(self, step):
        super(DC, self).__init__()
        if step % 2 == 0:
            pname = "_P1"
            sname = "_S1"
        else:
            pname = "_P2"
            sname = "_S2"

        self.add(mb.Particle(name=pname, pos=[-0.0628, 0.8896, 0.2186]), label=pname)
        self.add(mb.Particle(name=sname, pos=[0.2365, 0.6568, 0.1280]), label=sname)
        self.add(mb.Particle(name="_Cb1", pos=[0.0199, 0.2287, 0.0187]), label="_Cb1")
        self.add_bond((self[sname], self["_Cb1"]))
        self.add_bond((self[sname], self[pname]))  
        

class CDA(mb.Compound):
    def __init__(self, step):
        super(CDA, self).__init__()
        if step % 2 == 0:
            pname = "_P1"
            sname = "_S1"
        else:
            pname = "_P2"
            sname = "_S2"

        self.add(mb.Particle(name=pname, pos=[-0.0628, -0.8896, -0.2186]), label=pname)
        self.add(mb.Particle(name=sname, pos=[0.2365, -0.6568, -0.1280]), label=sname)
        self.add(mb.Particle(name="_Ab2", pos=[0.0575, -0.0516, -0.0051]), label="_Ab2")
        self.add_bond((self[sname], self["_Ab2"]))
        self.add_bond((self[sname], self[pname]))  
        
       
class CDT(mb.Compound):
    def __init__(self, step):
        super(CDT, self).__init__()
        if step % 2 == 0:
            pname = "_P1"
            sname = "_S1"
        else:
            pname = "_P2"
            sname = "_S2"

        self.add(mb.Particle(name=pname, pos=[-0.0628, -0.8896, -0.2186]), label=pname)
        self.add(mb.Particle(name=sname, pos=[0.2365, -0.6568, -0.1280]), label=sname)
        self.add(mb.Particle(name="_Tb2", pos=[0.0159, -0.2344, -0.0191]), label="_Tb2")
        self.add_bond((self[sname], self["_Tb2"]))
        self.add_bond((self[sname], self[pname]))  
        
 
class CDG(mb.Compound):
    def __init__(self, step):
        super(CDG, self).__init__()
        if step % 2 == 0:
            pname = "_P1"
            sname = "_S1"
        else:
            pname = "_P2"
            sname = "_S2"

        self.add(mb.Particle(name=pname, pos=[-0.0628, -0.8896, -0.2186]), label=pname)
        self.add(mb.Particle(name=sname, pos=[0.2365, -0.6568, -0.1280]), label=sname)
        self.add(mb.Particle(name="_Gb2", pos=[0.0628, -0.0540, -0.0053]), label="_Gb2")
        self.add_bond((self[sname], self["_Gb2"]))
        self.add_bond((self[sname], self[pname]))  
               

class CDC(mb.Compound):
    def __init__(self, step):
        super(CDC, self).__init__()
        if step % 2 == 0:
            pname = "_P1"
            sname = "_S1"
        else:
            pname = "_P2"
            sname = "_S2"
    
        self.add(mb.Particle(name=pname, pos=[-0.0628, -0.8896, -0.2186]), label=pname)
        self.add(mb.Particle(name=sname, pos=[0.2365, -0.6568, -0.1280]), label=sname)
        self.add(mb.Particle(name="_Cb2", pos=[0.0199, -0.2287, -0.0187]), label="_Cb2")
        self.add_bond((self[sname], self["_Cb2"]))
        self.add_bond((self[sname], self[pname]))  

class DNA(mb.Compound):
    def __init__(self, dna_str):
        super(DNA, self).__init__()
        n = 0

        # Check the string
        if len(dna_str.upper().replace("A","").replace("T","").replace("G","").replace("C","")):
            raise SystemError("Error: Please enter only bases A,T,C,G")

        else:
            # Add bases according to sequence
            for base in dna_str.upper():
                if n % 2 == 0:  
                    pname = "_P1"
                    sname = "_S1"
                    lastp = "_P2"
                    lasts = "_S2"
                else:
                    pname = "_P2"
                    sname = "_S2"
                    lastp = "_P1"
                    lasts = "_S1"


                if base == "A":
                    self.add(DA(n), label=str(n))
                    self.add(CDT(n), label="c"+str(n))
                elif base == "T":
                    self.add(DT(n), label=str(n))
                    self.add(CDA(n), label="c"+str(n))
                elif base == "G":
                    self.add(DG(n), label=str(n))
                    self.add(CDC(n), label="c"+str(n))
                else:
                    self.add(DC(n), label=str(n))
                    self.add(CDG(n), label="c"+str(n))

                # Rotate and translate bases
                self[str(n)].translate([0, 0, 0.338*n])
                self[str(n)].rotate(np.deg2rad(36*n),[0,0,1]) # rotate(theta,around)
 
                self["c"+str(n)].translate([0, 0, 0.338*n])
                self["c"+str(n)].rotate(np.deg2rad(36*n),[0,0,1])
                
                # Make bonds
                if n > 0:
                    self.add_bond((self[str(n)][sname],self[str(n-1)][lastp]))
                    self.add_bond((self["c"+str(n)][pname],self["c"+str(n-1)][lasts]))
                    
                n += 1

        # Translate molecule down to fix box        
        self.translate([0, 0, -0.338*n / 2.0])
 
                
if __name__ == "__main__":
    # Coarse grain DNA builder
    
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
    parser.add_argument("-i", "--input_string", 
                        type=str,
                        default="atgc",
                        required=False,
                        help="DNA sequence in in the one-letter way of specifying bases, e.g., ATGC")

    args = parser.parse_args()
    
    dna = DNA(args.input_string)
    
    fname = "dna.hoomdxml"
    if os.path.isfile(fname):
        if input("File " + fname + " already exists. Overwrite? (y/n):").upper() == "Y":
            os.remove(fname)
        else:
            print("File not overwritten")
            exit()
    
    dna.save(fname, forcefield_files="dna_cg-ff.xml")

    # The following is to increase the size of the box by factor of scale
    tree = ET.parse(fname)
    root = tree.getroot()

    scale = 4.0

    lx =  root[0][0].attrib["Lx"]
    ly =  root[0][0].attrib["Ly"]
    lz =  root[0][0].attrib["Lz"]

    # dict
    newbox = { 
        "Lx": str(scale * float(lx)),
        "Ly": str(scale * float(ly)),
        "Lz": str(scale * float(lz)),
        "units": "sigma" 
        } 

    root[0][0].attrib = newbox

    tree.write(fname)

    print(args.input_string + " DNA structure written to " + fname)


